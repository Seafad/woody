﻿using Mediator;
using UnityEngine;

/// <summary>
/// Main game class. Controls game logic
/// </summary>
public class GameController : Mediator.Mediator
{

    public WoodyBehaviourModule WoodyModule;
    public PlayerInputModule PlayerInputModule;
    public UIModule UIModule;

    private LevelModel model;
    private LevelInfo levelInfo;
    private Note[] userInput;

    public override void Send(string msg, Module module, object[] data)
    {
        if (module == WoodyModule)
        {
            switch (msg)
            {
                case "startPecking":
                    Debug.Log("PEcking");
                    UIModule.DisableGameUI();
                    PlayerInputModule.Notify("reset", new object[] { model.notes.Length, model.totalTime + 0.5f });
                    break;
                case "stopPecking":
                    UIModule.EnableGameUI();
                    break;
                case "finishScoreReaction":
                    UIModule.ShowResultScreen();
                    break;

            }
        }
        else if (module == PlayerInputModule)
        {
            switch (msg)
            {
                case "touched":
                    if (!WoodyModule.IsPecking)
                    {
                        PlayerInputModule.StartRecording();
                        UIModule.DisableGameUI();
                    }
                    break;
                case "inputFinished":
                    userInput = (Note[])data[0];
                    float score = CalculateScore();
                    UIModule.EnableGameUI();
                    WoodyModule.Notify("score", new object[] { score });
                    SaveResults(score);
                    UIModule.Notify("setScore", new object[] { score });
                    break;
            }
        }
        else if (module == UIModule)
        {
            switch (msg)
            {
                case "left_bound":
                    PlayerInputModule.Notify("init", data);
                    break;
            }
        }
    }

    void Start()
    {
        WoodyModule.SetMediator(this);
        PlayerInputModule.SetMediator(this);
        UIModule.SetMediator(this);
        WoodyModule.Notify("init");
        UIModule.Notify("init");
        ResetLevel();
    }

    private void ResetLevel()
    {
        levelInfo = new LevelInfo();
        model = GameDataManager.instance.GetCurrLevelModel();
        PlayerInputModule.Notify("reset", new object[] { model.notes.Length, model.totalTime + 0.5f });
        UIModule.Notify("reset", new object[] { model.title, (model.index + 1).ToString() });
        WoodyModule.Notify("reset", new object[] { model.notes });
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UIModule.Notify("togglePause");
        }
    }

    float CalculateScore()
    {
        float variance = 0;
        for (int i = 0; i < userInput.Length; i++)
            variance += System.Math.Abs(userInput[i] - model.notes[i]);
        return Mathf.Clamp01(1 - variance / (model.totalTime + 0.5f));
    }

    private void SaveResults(float score)
    {
        levelInfo.score = score;
        levelInfo.rating = RatingFromScore(score);
        levelInfo.isAvaliable = true;
        GameDataManager.instance.UpdateCurrLevelInfo(levelInfo);
    }

    private byte RatingFromScore(float score)
    {
        if (score < 0.75f) return 0;
        else if (score < 0.80f) return 1;
        else if (score < 0.95f) return 2;
        else return 3;
    }

    public void Replay()
    {
        ResetLevel();
    }

    public void NextLevel()
    {
        GameDataManager.instance.LoadLevel(GameDataManager.instance.CurrentLevel + 1);
        ResetLevel();
    }

    public void ToMenu()
    {
        GameSceneManager.instance.LoadMenu();
    }

    public void Resume()
    {
        UIModule.Notify("togglePause");
    }

    void OnDestroy()
    {
        SoundManager.instance.StopSounds();
    }
}
