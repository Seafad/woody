﻿using UnityEngine;

namespace Mediator
{
    public abstract class Mediator : MonoBehaviour
    {
        public abstract void Send(string msg, Module module, object[] data = null);
    }
}

