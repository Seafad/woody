﻿using System;
using Mediator;
using UnityEngine;

/// <summary>
/// Records player taps
/// </summary>
public class PlayerInputModule : Module
{
    public AudioClip PlayerClick;
    public Note[] noteArr;
    public float maxTime;

    private float time;
    private float totalTime;
    private int currNote;
    private bool isRecording;
    public float LEFT_BOUND;

    public override void Notify(string message, object[] data)
    {
        switch (message)
        {
            case "init":
                {
                    Init((float)data[0]);
                    break;
                }
            case "reset":
                {
                    Reset((int)data[0], (float)data[1]);
                    break;
                }  
        }
    }

    private void Init(float leftBound)
    {
        LEFT_BOUND = leftBound;
    }

    private void Reset(int noteArrLength, float maxTime)
    {
        noteArr = new Note[noteArrLength];
        currNote = 0;
        totalTime = 0;
        isRecording = false;
        this.maxTime = maxTime;
    }

    void Update()
    {
        if (isTouched() && !isRecording)
        {
            mediator.Send("touched", this);
            return;
        }
        if (isRecording)
        {
            totalTime += Time.deltaTime;
            if (currNote < noteArr.Length)
            {
                time += Time.deltaTime;
                if (isTouched())
                {
                    RecordTouch();
                }
            }

            if(totalTime > maxTime)
            {
                for (int i = currNote; i < noteArr.Length; i++)
                    noteArr[i].duration = 0;
                currNote = 0;
                totalTime = 0;
                isRecording = false;
                mediator.Send("inputFinished", this, new object[] { noteArr });
            }
        }  
    }

    internal void StartRecording()
    {
        SoundManager.instance.PlayEffect(PlayerClick);
        isRecording = true;
    }

    private void RecordTouch()
    {
        SoundManager.instance.PlayEffect(PlayerClick);
        noteArr[currNote++].duration = time;
        time = 0;
    }

    private bool isTouched()
    {
        if (Application.isMobilePlatform)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began && InClickArea())
                    return true;
            }
        }
        else if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0) && InClickArea())
            {
                return true;
            }
        }

        return false;
    }

    private bool InClickArea()
    {
        return Input.mousePosition.x > LEFT_BOUND;
    }
}
