﻿using UnityEngine;

namespace Mediator
{
    public abstract class Module : MonoBehaviour
    {
        protected Mediator mediator;

        public void SetMediator(Mediator mediator)
        {
            this.mediator = mediator;
        }
        public virtual void Send(string message)
        {
            mediator.Send(message, this);
        }
        public abstract void Notify(string message, object[] data = null);
    }

}
