﻿using System;
using Mediator;
using UnityEngine;
using UnityEngine.UI;

public class UIModule : Module
{
    public GameObject Tree;
    public GameObject ResultMenu;
    public GameObject PauseMenu;
    public GameObject Game;
    public Button NextButton;
    public GameObject ReplayButton;
    public Text LevelTitle;
    public Text LevelNumText;
    public AudioClip loseSound, winSound;
    private Text menuText;

    private bool isResultShown;

    public override void Notify(string message, object[] data = null)
    {
        switch (message)
        {
            case "init":
                Init();
                break;
            case "reset":
                Reset((string)data[0], data[1] as string);
                break;
            case "togglePause":
                TogglePause();
                break;
            case "setScore":
                SetScore((float)data[0]);
                break;
        }
    }

    private void Init()
    {
        menuText = ResultMenu.GetComponentInChildren<Text>();
        SoundManager.instance.StopMusic();

        float treeSpriteWidth = Tree.GetComponent<Renderer>().bounds.size.x;
        Vector3 rightEdge = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        Tree.transform.position = new Vector2(rightEdge.x - treeSpriteWidth / 2, Tree.transform.position.y);
        float left_bound = Camera.main.WorldToScreenPoint(new Vector3(Tree.transform.position.x, 0, 0)).x;

        mediator.Send("left_bound", this, new object[] { left_bound });
    }

    private void Reset(string levelTitle, string levelNum)
    {
        LevelTitle.text = levelTitle;
        LevelNumText.text = levelNum;
        isResultShown = false;
        ReplayButton.SetActive(true);
        ResultMenu.SetActive(false);
        Game.SetActive(true);
        Resume();
    }

    public void DisableGameUI()
    {
        ReplayButton.SetActive(false);
    }

    public void EnableGameUI()
    {
        ReplayButton.SetActive(true);
    }

    private void TogglePause()
    {
        if (Time.timeScale == 1)
            Pause();
        else Resume();
    }

    private void Pause()
    {
        Time.timeScale = 0;
        if (!GameDataManager.instance.IsNextAvaliable())
            NextButton.interactable = false;
        else
            NextButton.interactable = true;
        Game.SetActive(false);
        PauseMenu.SetActive(true);
    }

    private void Resume()
    {
        Time.timeScale = 1;
        Game.SetActive(true);
        PauseMenu.SetActive(false);
    }

    private void SetScore(float score)
    {
        if (score < 0.75f)
        {
            menuText.text = string.Format(getRandomLoseString(), (int)(score * 100));
            SoundManager.instance.PlayEffect(loseSound);
        }
        else
        {
            menuText.text = string.Format(getRandomWinString(), (int)(score * 100));
            SoundManager.instance.PlayEffect(winSound);
        }
        if (!GameDataManager.instance.IsNextAvaliable())
            NextButton.interactable = false;
        else
            NextButton.interactable = true;
    }

    public void ShowResultScreen()
    {
        Game.SetActive(false);
        ResultMenu.SetActive(true);
    }

    private string getRandomLoseString()
    {
        return Const.loseStrings[(int)UnityEngine.Random.Range(0, Const.loseStrings.Length)];
    }
    private string getRandomWinString()
    {
        return Const.winStrings[(int)UnityEngine.Random.Range(0, Const.winStrings.Length)];
    }

}

