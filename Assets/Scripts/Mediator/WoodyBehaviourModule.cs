﻿using Mediator;
using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Controls the woodpecker 
/// </summary>
public class WoodyBehaviourModule : Module
{
    public AudioClip PeckSound;
    public AnimationClip PeckAnimation;
    public Note[] noteArr;
    public bool IsPecking { get; private set; }
    private float time;
    private int currNote;
    private bool isPecking;
    private Animator Animator;
    private int idle, peck, lose, win;

    public override void Notify(string message, object[] data = null)
    {
        switch (message)
        {
            case "init":
                Init();
                break;
            case "reset":
                Reset((Note[])data[0]);
                break;
            case "score":
                ReactToScore((float)data[0]);
                break;
        }
    }

    void Init()
    {
        currNote = 0;
        Animator = GetComponent<Animator>();
        isPecking = false;
        idle = Animator.StringToHash("Idle");
        peck = Animator.StringToHash("Peck");
        lose = Animator.StringToHash("Lose");
        win = Animator.StringToHash("Win");
    }

    private void Reset(Note[] newNotes)
    {
        isPecking = false;
        noteArr = newNotes;
        currNote = 0;
        time = 0;
        Animator.Play(idle);
    }

    private void ReactToScore(float score)
    {
        if (score < 0.75f)
            Animator.Play(lose);
        else
            Animator.Play(win);
        StartCoroutine(WaitForAnimToFinish());
        mediator.Send("finishScoreReaction", this);
    }

    void Update()
    {
        if (isPecking)
        {
            if (time == 0) Peck();
            time += Time.deltaTime;
            if (currNote < noteArr.Length)
            {
                if (time >= noteArr[currNote].duration)
                {
                    time = 0;
                    currNote++;
                }
            }
            else
            {
                Reset(noteArr);
                mediator.Send("stopPecking", this);
            }
        }
    }

    IEnumerator WaitForAnimToFinish()
    {
        while (IsAnimPlaying())
        {
            yield return WaitForAnimToFinish();
        }
    }

    void Peck()
    {
        Animator.Play(peck);
        SoundManager.instance.PlayEffect(PeckSound);
    }



    public void StartPecking()
    {
        if (!isPecking)
        {
            mediator.Send("startPecking", this);
            isPecking = true;
        }
    }

    private bool IsAnimPlaying()
    {
        return Animator.GetCurrentAnimatorStateInfo(0).length > Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }


}