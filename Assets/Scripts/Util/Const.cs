﻿class Const
{
    public static string[] loseStrings = { "Awh, it was about {0}% right", "I'm sure you can do better than {0}%", "This tree didn't even feel the PAAAIN ({0}%)", "{0}%? Pfff...", "{0}%? Woody, try agian!" };
    public static string[] winStrings = { "Wow, it was {0}% right", "Great! {0}%", "You killed that tree! {0}%!", "{0}%?! That was cool.", "{0}%? One more time, please?" };
}
