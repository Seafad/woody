﻿
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/// <summary>
/// Does "Low-level" data-layer file actions
/// </summary>
class FileManager
{
    private static string SAVEFILE_PATH = Application.persistentDataPath + "/gamedata.dat";

    public static void SaveData(GameData DataToSave)
    {
        FileStream saveFile = File.Open(SAVEFILE_PATH, FileMode.Create);
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(saveFile, DataToSave);
        saveFile.Close();
    }

    public static GameData LoadData()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Open(SAVEFILE_PATH, FileMode.Open);
        GameData gameData = (GameData)formatter.Deserialize(saveFile);
        saveFile.Close();
        return gameData;

    }

    public static void ReinitData(int levelCount, bool forceErase)
    {
        if (forceErase || !File.Exists(SAVEFILE_PATH))
        {
            List<LevelInfo> levelInfos = new List<LevelInfo>();
            for (int i = 0; i < levelCount; i++)
                levelInfos.Add(new LevelInfo());
            levelInfos[0].isAvaliable = true;

            GameData initialGameData = new GameData()
            {
                levelInfos = levelInfos,
                totalTimePlayed = 0,
                lastLevel = 0
            };

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream saveFile = File.Open(SAVEFILE_PATH, FileMode.Create);
            formatter.Serialize(saveFile, initialGameData);
            saveFile.Close();
        }
    }
}



