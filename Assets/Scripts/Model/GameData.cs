﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// contains all dynamic data that needs to be stored between game launches
/// </summary>
[Serializable]
public class GameData
{
    [SerializeField]
    public IList<LevelInfo> levelInfos;
    public int lastLevel;
    public float totalTimePlayed;
}

/// <summary>
/// contains dynamic information about level
/// </summary>
[Serializable]
public class LevelInfo
{
    public byte rating = 0;
    public float score = 0;
    public bool isAvaliable = false;
}

/// <summary>
/// contains pre-defined characteristics for levels
/// should BE accessible on first start up
/// </summary>
[Serializable]
public class GameDataStatic
{
    [HideInInspector]
    public int numOfLevels;
    [SerializeField]
    public List<LevelModel> levelModels;
}

/// <summary>
/// contains static information about level
/// </summary>
[Serializable]
public class LevelModel
{
    [HideInInspector]
    public int index;
    public string title;
    [SerializeField]
    public Note[] notes;
    [HideInInspector]
    public float totalTime;
}

[Serializable]
public struct Note
{
    public float duration;
    public static float operator - (Note note1, Note note2) {
        return note1.duration - note2.duration;
    }
}

