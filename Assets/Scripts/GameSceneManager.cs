﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSceneManager : MonoBehaviour
{

    public static GameSceneManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
    }


    public void LoadLevel(int levelIndex)
    {
        GameDataManager.instance.LoadLevel(levelIndex);
        SceneManager.LoadScene("Game");
    }

    internal void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
