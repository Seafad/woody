﻿using System;
using UnityEngine;

/// <summary>
/// Single access point for all data stored between launches.
/// </summary>
public class GameDataManager : MonoBehaviour
{

    [SerializeField]
    private GameData gameData; //contains dynamic data (e.g. scores, total time played) - initialized on first launch
    [SerializeField]
    private GameDataStatic gameDataStatic; //contains static data (e.g. level maps, number of levels) - packaged with game, avaliable on first launch.
    private int currentLevel;

    public static GameDataManager instance;
    public int CurrentLevel { get { return currentLevel; } private set { } }
    public GameData GameData { get { return gameData; } private set { } }//TODO: find out why properties don't return fields that are classes without explicit get method declaration?
    public GameDataStatic GameDataStatic { get { return gameDataStatic; } private set { } }

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
            ReinitData(false);
            gameData = FileManager.LoadData();
        }
        else if (instance != this)
            Destroy(gameObject);
    }

    public void ReinitData(bool forceErase)
    {
        FileManager.ReinitData(gameDataStatic.numOfLevels, forceErase);
    }

    public LevelModel GetCurrLevelModel()
    {
        return gameDataStatic.levelModels[currentLevel];
    }

    public void UpdateCurrLevelInfo(LevelInfo newInfo)
    {
        LevelInfo storedInfo = gameData.levelInfos[currentLevel];
        if (newInfo.score > storedInfo.score)//updating level info only if new result is better
        {
            gameData.levelInfos[currentLevel] = newInfo;
            if (IsNextAvaliable())
            {
                gameData.lastLevel = currentLevel + 1;
                gameData.levelInfos[currentLevel + 1].isAvaliable = true;
            }
            Save();
        }
    }

    public void LoadLevel(int levelIndex)
    {
        currentLevel = levelIndex;
    }

    public bool IsNextAvaliable()
    {
        return currentLevel + 1 < gameDataStatic.numOfLevels && gameData.levelInfos[currentLevel].rating > 0;
    }

    private void Save()
    {
        FileManager.SaveData(gameData);
    }


}

