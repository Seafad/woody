﻿using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public GameObject StartGame;
    public GameObject ContinueGame;
    public GameObject MainMenu;
    public GameObject LevelSelect;
    public AudioClip MenuMusic;

    void Start()
    {
        SoundManager.instance.SetMusic(MenuMusic);
        if(GameDataManager.instance.GameData.lastLevel == 0)
        {
            ContinueGame.SetActive(false);
        }
        else
        {
            StartGame.SetActive(false);
        }
    }

    public void ToLevelSelect()
    {
        MainMenu.SetActive(false);

        LevelSelect.SetActive(true);
    }

    public void ToMainMenu()
    {
        MainMenu.SetActive(true);
        LevelSelect.SetActive(false);
    }

    public void Continue()
    {
        GameSceneManager.instance.LoadLevel(GameDataManager.instance.GameData.lastLevel);
    }

}