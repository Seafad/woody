﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Fills Level prefab with data from model
/// </summary>
public class LevelView : MonoBehaviour
{

    public Text levelNumberText;
    public Scrollbar progressBar;
    public Text levelTitleText;
    public Image[] note;
    public Button playButton;
    private static Color32 initialColor = new Color32(0x00, 0x00, 0x00, 0x7F);

    private const string TITLE_UNKNOWN = "? ? ?";

    public void Display(LevelInfo levelInfo, LevelModel levelModel)
    {
        levelNumberText.text = (levelModel.index + 1).ToString();
        progressBar.value = levelInfo.score;
        levelTitleText.text = levelInfo.isAvaliable ? levelModel.title : TITLE_UNKNOWN;
        for (int i = 0; i < levelInfo.rating; i++)
        {
            note[i].color = Color.black;
        }
        for (int i = levelInfo.rating; i < 3; i++)
        {
            note[i].color = initialColor;
        }
        if (levelInfo.isAvaliable)
        {
            playButton.onClick.AddListener(() => GameSceneManager.instance.LoadLevel(levelModel.index));
        }
        else
        {
            playButton.interactable = false;
        }
        //TODO: why is localScale set to a weird value on init, if not forced to one? (CanvasScaler expected behaviour?)
        gameObject.transform.localScale = Vector3.one;
    }

}
