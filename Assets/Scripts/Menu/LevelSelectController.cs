﻿using System.Collections.Generic;
using UnityEngine;

public class LevelSelectController : MonoBehaviour {

    public GameObject LevelPrefab;
    public Transform ScrollViewContent;
    public ObjectPool pool;


    void OnEnable()
    {
        DisplayLevels();
    }

    void OnDisable()
    {
        RemoveLevels();
    }

    private void DisplayLevels()
    {
        int levelCount = GameDataManager.instance.GameDataStatic.numOfLevels;
        IList<LevelInfo> levelInfos = GameDataManager.instance.GameData.levelInfos;
        IList<LevelModel> levelModels = GameDataManager.instance.GameDataStatic.levelModels;
        for (int i = 0; i < levelCount; i++)
        {
            GameObject levelPrefab = pool.GetObject();
            levelPrefab.transform.SetParent(ScrollViewContent);
            LevelView levelView = levelPrefab.GetComponent<LevelView>();
            levelView.Display(levelInfos[i], levelModels[i]);
        }
    }

    private void RemoveLevels()
    {
        while (ScrollViewContent.childCount > 0) {
            GameObject toRemove = ScrollViewContent.GetChild(0).gameObject;
            pool.ReturnObject(toRemove);
        }
    }

}
