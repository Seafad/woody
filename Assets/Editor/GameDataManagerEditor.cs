﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
/// <summary>
/// This custom editor helps to update some fields that would be tiring to update manually
/// P.S. Storing design-time data using XML is better. I now see it.
/// </summary>
[CustomEditor(typeof(GameDataManager))]
public class GameDataManagerEditor : Editor
{
    private GameDataManager script;
    private List<LevelModel> levelModels;
    private bool isApplyButtonDisabled;

    void OnEnable()
    {
        script = (GameDataManager)target;
        levelModels = script.GameDataStatic.levelModels;
    }

    //here it gets nasty
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        {
            EditorGUILayout.LabelField("Number of levels", script.GameDataStatic.numOfLevels.ToString());
            DrawDefaultInspector();
        }
        if (EditorGUI.EndChangeCheck())
        {
            isApplyButtonDisabled = false;
        }
        for (int i = 0; i < levelModels.Count; i++)
        {
            levelModels[i].index = i;
            float total = 0;
            for (int j = 0; j < levelModels[i].notes.Length; j++)
                total += levelModels[i].notes[j].duration;
            levelModels[i].totalTime = total;
        }
        EditorGUI.BeginDisabledGroup(isApplyButtonDisabled);
        if (GUILayout.Button("Apply changes"))
        {
            script.GameDataStatic.numOfLevels = levelModels.Count;
            isApplyButtonDisabled = true;
        }
        EditorGUI.EndDisabledGroup();
    }
}
